//
//  AAAManager.m
//  AAA
//
//  Created by tengpan on 2017/8/4.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import "AAAManager.h"
#import <BBB/BBB.h>
#import <CCC/CCC.h>

@implementation AAAManager

- (void)test
{
    NSLog(@"%@",NSStringFromClass([self class]));
    [[[BBBManager alloc] init] test];
    [[[CCCManager alloc] init] test];
}

@end
